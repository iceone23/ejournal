CREATE TABLE IF NOT EXISTS topics (
    topic_id INT AUTO_INCREMENT PRIMARY KEY,
    topic_date VARCHAR(25),
    topic_type VARCHAR(25),
    topic_num VARCHAR(10),
    topic_name VARCHAR(255),
    subject_id INT,
    FOREIGN KEY (subject_id) REFERENCES subjects(subject_id)
);