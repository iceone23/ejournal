CREATE TABLE IF NOT EXISTS visits (
    visit_id INT AUTO_INCREMENT PRIMARY KEY,
    person_id INT,
    group_id INT,
    topic_id INT,
    subject_id INT,
    visit_report BOOLEAN,
    FOREIGN KEY (person_id) REFERENCES persons(person_id),
    FOREIGN KEY (group_id) REFERENCES groups(group_id),
    FOREIGN KEY (topic_id) REFERENCES topics(topic_id),
    FOREIGN KEY (subject_id) REFERENCES subjects(subject_id)
);