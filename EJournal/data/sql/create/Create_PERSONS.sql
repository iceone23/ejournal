CREATE TABLE IF NOT EXISTS persons (
    person_id INT AUTO_INCREMENT PRIMARY KEY,
    person_name VARCHAR(100),
    group_id INT,
    FOREIGN KEY (group_id) REFERENCES groups(group_id)
);