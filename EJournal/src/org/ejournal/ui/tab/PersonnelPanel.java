package org.ejournal.ui.tab;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

public class PersonnelPanel extends JPanel {
	private static final long serialVersionUID = -4709476285549537451L;
	
	private JTable personnelTable;

	/**
	 * Create the panel.
	 */
	public PersonnelPanel() {
		setBorder(new EmptyBorder(0, 5, 0, 5));
		setLayout(new BorderLayout(0, 0));
		
		JPanel groupPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) groupPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		add(groupPanel, BorderLayout.NORTH);
		
		JComboBox groupCb = new JComboBox();
		groupPanel.add(groupCb);
		
		JPanel personnelActionPanel = new JPanel();
		add(personnelActionPanel, BorderLayout.SOUTH);
		personnelActionPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JButton addPersonButton = new JButton("Додати");
		personnelActionPanel.add(addPersonButton);
		
		JButton editPersonButton = new JButton("Редагувати");
		personnelActionPanel.add(editPersonButton);
		
		JButton delPersonButton = new JButton("Видалити");
		personnelActionPanel.add(delPersonButton);
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		personnelTable = new JTable();
		scrollPane.setViewportView(personnelTable);

	}

}
