package org.ejournal.ui.tab;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;

import org.ejournal.db.Storage;
import org.ejournal.db.obj.Group;
import org.ejournal.db.tablemodel.GroupTableModel;
import javax.swing.border.EmptyBorder;

public class GroupPanel extends JPanel {
	private static final long serialVersionUID = -275089845261163260L;
	
	private Storage storage;
	
	private List<Group> cash = new ArrayList<Group>();
	protected Map<Character, List<Group>> groupsByAlphabet = new HashMap<Character, List<Group>>();

	private JTable groupTable;

	/**
	 * Create the panel.
	 */
	public GroupPanel(Storage storage) {
		setBorder(new EmptyBorder(5, 5, 0, 5));
		this.storage = storage;
		
		setLayout(new BorderLayout(0, 0));
		
		JPanel groupActionPanel = new JPanel();
		add(groupActionPanel, BorderLayout.SOUTH);
		groupActionPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JButton addGroupButton = new JButton("Додати");
		addGroupButton.addActionListener(new InsertItemListener());
		groupActionPanel.add(addGroupButton);
		
		JButton delGroupButton = new JButton("Видалити");
		delGroupButton.addActionListener(new DeleteItemListener());
		groupActionPanel.add(delGroupButton);
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		groupTable = new JTable();
		groupTable.setBorder(new LineBorder(Color.GREEN));
		scrollPane.setViewportView(groupTable);

		updateGroupList();
	}
	
	public void updateGroupList() {
		if (storage != null) {
			cash = storage.getGroups();
			List<Group> source = new ArrayList<Group>(cash);
			setTableModel(source);
		}
	}
	
	protected void updateGroupsByAlphabet() {
		groupsByAlphabet.clear();
		for(Group a : cash) {
			if (a.getName().isEmpty()) {
				continue;
			}
			
			if (groupsByAlphabet.get(a.getName().charAt(0)) == null) {
				groupsByAlphabet.put(Character.toUpperCase(a.getName().charAt(0)), new ArrayList<Group>());
			}
			groupsByAlphabet.get(Character.toUpperCase(a.getName().charAt(0))).add(a);
		}
	}
	
	class InsertItemListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if (storage != null) {
				getModel().addEmtyItem();
				updateGroupList();
			}
		}
	}
	
	class DeleteItemListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			int rowIndex = groupTable.getSelectedRow();
			if (rowIndex >= 0) {
				getModel().deleteItemAt(rowIndex);
				updateGroupList();
			} else {
				JOptionPane.showMessageDialog(GroupPanel.this, "Елемент для видалення не вибрано!");
			}
		}
	}
	
	public void setTableModel(List<Group> modelData) {
		GroupTableModel model = new GroupTableModel(modelData);
		groupTable.setModel(model);
	}
	
	private GroupTableModel getModel() {
		return (GroupTableModel) groupTable.getModel();
	}

}
