package org.ejournal.ui.datepicker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.qt.datapicker.DatePicker;

public class DatePickerPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private ObservingTextField textField;

	/**
	 * Create the panel.
	 */
	public DatePickerPanel() {
		setLayout(null);
		
		textField = new ObservingTextField();
		textField.setBounds(0, 0, 114, 19);
		add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("...");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				String lang = null;
				final Locale locale = getLocale(lang);
				DatePicker dp = new DatePicker(textField, locale);
				
				//previously selected date
			    Date selectedDate = dp.parseDate(textField.getText());
				dp.setSelectedDate(selectedDate);
				dp.start(textField);
			}
		});
		btnNewButton.setBounds(115, 0, 22, 19);
		add(btnNewButton);

	}
	
	private Locale getLocale(String loc) {
		if (loc != null && loc.length() > 0)
			return new Locale(loc);
		else
			return Locale.US;
	}
	
	public String getDate() {
		return textField.getText();
	}

}
