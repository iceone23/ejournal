package org.ejournal.ui.datepicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextField;

public class ObservingTextField extends JTextField implements Observer {
	private static final long serialVersionUID = 4515037284112518700L;

	public void update(Observable o, Object arg) {
        Calendar calendar = (Calendar) arg;
        //DatePicker dp = (DatePicker) o;
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                
        setText(dateFormat.format(calendar.getTime()));
    }

}