package org.ejournal.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import org.ejournal.db.Storage;
import org.ejournal.ui.tab.GroupPanel;
import org.ejournal.ui.tab.PersonnelPanel;

public class EJournalFrame extends JFrame {
	private static final long serialVersionUID = 4003678787681212285L;

	private Storage storage;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EJournalFrame window = new EJournalFrame();
					window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public EJournalFrame() {
		setTitle("EJournal");
		initialize();
	}

	private void initialize() {
		this.storage = Storage.getInstance("lesson-visits-stat");
		
		setSize(600, 450);
		setLocationRelativeTo(null);
		
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		PersonnelPanel personnelPanel = new PersonnelPanel();
		tabbedPane.addTab("Особовий склад", null, personnelPanel, null);
		
		GroupPanel groupPanel = new GroupPanel(storage);
		tabbedPane.addTab("Групи", null, groupPanel, null);
	}
}
