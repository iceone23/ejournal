package org.ejournal.db.tablemodel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import org.ejournal.db.Storage;
import org.ejournal.db.obj.Group;

public class GroupTableModel extends DefaultTableModel {
	private static final long serialVersionUID = 8157285056826181979L;
	private static final int COLUMNT_COUNT = 2;
	private static final String[] COLUMN_NAMES = {
		"id",
		"Група",};

	protected List<Group> groups = new ArrayList<Group>();
	
	public GroupTableModel(List<Group> groups) {
		this.groups.addAll(groups);
	}

	@Override
	public String getColumnName(int rowIndex) {
		return COLUMN_NAMES[rowIndex];
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0) {
			return Integer.class;
		} else {
			return String.class;
		}
	}

	@Override
	public int getColumnCount() {
		return COLUMNT_COUNT;
	}
	
	@Override
	public int getRowCount() {
		if (groups != null) {
			return groups.size();
		}
		return 0;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if ((rowIndex < 0) || (rowIndex >= groups.size())) {
			return null;
		}
		
		switch(columnIndex) {
		case 0 : return groups.get(rowIndex).getId();
		case 1 : return groups.get(rowIndex).getName();
		default : return null;
		}
	}
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		if ((rowIndex > 0) || (rowIndex <= groups.size())) {
			switch(columnIndex) {
			case 0 : 
				int newItemId = (Integer) value;
				if (groupIds().contains(newItemId)){
					JOptionPane.showMessageDialog(null, "Такий ідентифікатор вже існує!");
					return;
				}
				
				Storage.getInstance("lesson-visits-stat").updateGroup(groups.get(rowIndex));
				groups.get(rowIndex).setId(newItemId);
				break;
				
			case 1 : 
				groups.get(rowIndex).setName((String) value);
				Storage.getInstance("lesson-visits-stat").updateGroup(groups.get(rowIndex));
				break;
			}
			fireTableChanged(null);
		}
	}
	
	public List<Integer> groupIds() {
		List<Integer> groupIds = new ArrayList<Integer>();
		for (Group g : groups) {
			groupIds.add(g.getId());
		}
		
		return groupIds;
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public void addEmtyItem() {
		Group g = new Group();
		g.setName("Нова група");
		Storage.getInstance("lesson-visits-stat").insertGroup(g);

		fireTableChanged(null);
	}
	
	public void insertItemAt(int rowIndex) {
		
	}
	
	public void deleteItemAt(int rowIndex) {
		int id = groupIds().get(rowIndex);
		Storage.getInstance("lesson-visits-stat").deleteGroup(id);
		fireTableChanged(null);
	}
	
	public void deleteItem(int id) {
		for (int i = 0; i < groups.size(); i++) {
			if (groups.get(i).getId() == id) {
				groups.remove(i);
				return;
			}
		}
	}
	
}
