package org.ejournal.db.tablemodel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.ejournal.db.obj.Person;

/**
 * Клас для відображення списку о/с групи.
 * Підтримує можливість сортування о/с.
 */
public class AbstractPersonTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 8157285056826181979L;
	protected List<Person> personnel;
	
	public AbstractPersonTableModel(List<Person> personnel) {
		this.personnel = personnel;
	}

	public int getRowCount() {
		return personnel.size();
	}

	public int getColumnCount() {
		return 0;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return null;
	}
	
	/**
	 * Впорядковує о/с згідно алгоритму в comparator
	 * @param comparator алгоритм для впорядкування
	 */
	public void sort(Comparator<Person> comparator) {
		Collections.sort(personnel, comparator);
	}
	
	/**
	 * Вертає об’єкт типу {@link Person}
	 * 
	 * @param rowIndex номер рядка
	 */
	public Person getPersonAt(int rowIndex) {
		if ((rowIndex >= 0) && (rowIndex < personnel.size())) {
			return personnel.get(rowIndex);
		} else {
			return null;
		}
	}
	
	/**
	 * Повертає число о/с
	 */
	public int getPersonnelCount() {
		return personnel.size();
	}
	
	/**
	 * Повертає номер стрічки, де знаходиться данний курсант
	 * @param person
	 * @return
	 */
	public int getPersonRow(Person person) {
		for(int i = 0; i < personnel.size(); i++) {
			if (personnel.get(i).equals(person)) {
				return i;
			}
		}
		return -1;
	}
}
