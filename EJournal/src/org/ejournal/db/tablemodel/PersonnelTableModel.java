package org.ejournal.db.tablemodel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.ejournal.db.obj.Person;

public class PersonnelTableModel extends AbstractPersonTableModel {
	private static final long serialVersionUID = 8157285056826181979L;
	private static final int COLUMNT_COUNT = 2;
	private static final String[] COLUMN_NAMES = {	"id",
													"ФІО"};
	
	protected List<Person> personnel;
	
	public PersonnelTableModel(List<Person> personnel) {
		super(personnel);
	}

	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0) {
			return Integer.class;
		} else {
			return String.class;
		}
	}
	
	public int getColumnCount() {
		return COLUMNT_COUNT;
	}

	public String getColumnName(int rowIndex) {
		return COLUMN_NAMES[rowIndex];
	}
	
	public int getRowCount() {
		return personnel.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		if ((rowIndex < 0) || (rowIndex >= personnel.size())) {
			return null;
		}
		
		switch(columnIndex) {
		case 0 : return personnel.get(rowIndex).getId();
		case 1 : return personnel.get(rowIndex).getName();
		default : return null;
		}
	}
	
	public void sort(Comparator<Person> comparator) {
		Collections.sort(personnel, comparator);
	}
	
	public Person getePersonAt(int rowIndex) {
		if ((rowIndex >= 0) && (rowIndex < personnel.size())) {
			return personnel.get(rowIndex);
		} else {
			return null;
		}
	}
	
	public int getPersonCount() {
		return personnel.size();
	}
	
	public int getPersonRow(Person applicant) {
		for(int i = 0; i < personnel.size(); i++) {
			if (personnel.get(i).equals(applicant)) {
				return i;
			}
		}
		return -1;
	}
}
