package org.ejournal.db;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.ejournal.db.obj.Group;
import org.ejournal.db.obj.Person;
import org.ejournal.db.query.FileQueryManager;
import org.ejournal.db.query.QueryManager;
import org.ejournal.db.query.QueryNotFoundException;

/**
 * Клас для зберігання данних
 * Являє собою 'пул одинаків' (Singlenton pool)
 * Щоправда, одинак для одного імені
 * Тобто, якщо викликати метод getInstance() для двох різних імен, то будуть різні об'єкти
 * Для роботи використовує СУБД  H2
 */
public class Storage {
	/**
	 * Шлях до каталогу з даними програми. У 
	 * цьому каталозі знаходяться службові файли, які використовуються 
	 * даною програмою.
	 */
	public static String DATA_PATH = "." + File.separator + "data";
	private static Storage instance;
	private static Map<String, Storage> instances = new HashMap<String, Storage>();
	private String databaseFileName;
	private String storageName;
	
	private Connection connection;
	private Map<String, PreparedStatement> preparedSelects = new HashMap<String, PreparedStatement>();
	private Map<String, PreparedStatement> preparedInserts = new HashMap<String, PreparedStatement>();
	private Map<String, PreparedStatement> preparedDeletes = new HashMap<String, PreparedStatement>();
	private Map<String, PreparedStatement> preparedUpdates = new HashMap<String, PreparedStatement>();
	
	private QueryManager queryManager;

	/**
	 * Повертає назву файлу, у якому зберігається база данних.
	 */
	public String getStorageFilename() {
		return databaseFileName;
	}
	
	/**
	 * Повертає назву сховища
	 */
	public String getStorageName() {
		return storageName;
	}
	
	static {
		try {
			Class.forName("org.h2.Driver");
		} catch(ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Не вдається завантажити драйвер СУБД!");
			System.exit(0);
		}
	}
	
	/**
	 * Повертає екземпляр сховища
	 * 
	 * @param database назва сховища. Якщо БД з таким іменем не існує, воно буде створене
	 * 
	 * @return екземпляр сховища
	 */
	public static Storage getInstance(String database) {
		if(instances.containsKey(database)) {
			instance = instances.get(database);
		} else {
			instance = new Storage(database);
			instances.put(database, instance);
		}
		return instance;
	}
	
	public static Storage getCurrentStorage() {
		if (instance == null) {
			throw new IllegalStateException("Не було створено ні одного екземпляра бази данних для роботи!");
		}
		return instance;
	}
	
	private Storage(String database) {
		try {
			this.databaseFileName = database + ".h2.db";
			this.storageName = database;
			connection = DriverManager.getConnection("jdbc:h2:" + DATA_PATH + File.separator + database);
			queryManager = new FileQueryManager(DATA_PATH + File.separator);
			createDatabaseIfNotExists(database);
			prepareStatements();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Не вдається під'єднатися до БД " + database);
		}
	}
	
	private void createDatabaseIfNotExists(String database) {
		try {
			for(String tableName : queryManager.getTableNames()) {
				createTable(tableName);
			}
			
		} catch (QueryNotFoundException except) {
			JOptionPane.showMessageDialog(null, "Помилка при створенні таблиць БД. Подробиці " + except);
		}
	}
	
	private void createTable(String tableName) {
		try {
			String queryForCreateTable = queryManager.getCreateTableQuery(tableName);
			executeQuery(queryForCreateTable);
		} catch (QueryNotFoundException queryNotFound) {
			JOptionPane.showMessageDialog(null, "Помилка при створенні таблиці. Подробиці " + queryNotFound);
		} catch (SQLException sqlExcept) {
			JOptionPane.showMessageDialog(null, "Помилка при виконанні запиту на створення таблиці Подробиці " + sqlExcept);
		}
	}
	
	private void prepareStatements() {
		PreparedStatement preparedStatement = null;
		try {
			for(String tableName : queryManager.getTableNames()) {
				
				preparedStatement = connection.prepareStatement(queryManager.getSelectQuery(tableName));
				preparedSelects.put(tableName, preparedStatement);
				
				preparedStatement = connection.prepareStatement(queryManager.getInsertQuery(tableName));
				preparedInserts.put(tableName, preparedStatement);
				
				preparedStatement = connection.prepareStatement(queryManager.getUpdateQuery(tableName));
				preparedUpdates.put(tableName, preparedStatement);
				
				preparedStatement = connection.prepareStatement(queryManager.getDeleteQuery(tableName));
				preparedDeletes.put(tableName, preparedStatement);
			}
			
		} catch(SQLException except) {
			JOptionPane.showMessageDialog(null, "Помилка при створенні підготовлених запитів " + except);
		} catch (QueryNotFoundException e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	
	private void executeQuery(String query) throws SQLException {
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				System.out.println("Не вдалося звільнити statement");
			}
		}
	}
	
	public void insertPerson(Person p) {
		PreparedStatement st = null;
		try {
			st = preparedInserts.get("PERSONS");
			st.setString(1, p.getName());
			st.setLong(2, p.getGroupId());
			st.execute();
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося додати нову особу! " + except);
		}
	}
	
	public void updatePerson(Person p) {
		PreparedStatement st = null;
		try {
			st = preparedUpdates.get("PERSONS");
			st.setString(1, p.getName());
			st.setInt(2, p.getGroupId());
			st.setInt(3, p.getId());
			st.execute();
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося оновити інформацію! " + except);
		}
	}
	
	public void deletePerson(int personId) {
		PreparedStatement st = null;
		try {
			st = preparedDeletes.get("PERSONS");
			st.setInt(1, personId);
			st.execute();
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося видалити особу з БД!");
			System.exit(0);
		}
	}
	
	public Person getPerson(int personId) {
		Person p = new Person(personId);
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = preparedInserts.get("PERSONS");
			st.setInt(1, personId);
			st.execute();
			rs = st.getResultSet();
			
			p.setName(rs.getString("person_name"));
			p.setGroupId(rs.getInt("group_id"));
			
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося завантажити особу! " + except);
		}
		return p;
	}
	
	public List<Person> getPersonnel() {
		List<Person> group = new ArrayList<Person>();
		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT person_id FROM persons");
			while(rs.next()) {
				group.add(getPerson(rs.getInt("person_id")));
			}
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Помилка при завантаженні о/с!");
		}
		return group;
	}
	
	public void insertGroup(Group g) {
		PreparedStatement st = null;
		try {
			st = preparedInserts.get("GROUPS");
			st.setString(1, g.getName());
			st.execute();
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося додати нову групу! " + except);
		}
	}
	
	public void updateGroup(Group g) {
		PreparedStatement st = null;
		try {
			st = preparedUpdates.get("GROUPS");
			st.setString(1, g.getName());
			st.setInt(2, g.getId());
			st.execute();
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося оновити інформацію! " + except);
		}
	}
	
	public void deleteGroup(int groupId) {
		PreparedStatement st = null;
		try {
			st = preparedDeletes.get("GROUPS");
			st.setInt(1, groupId);
			st.execute();
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося видалити групу з БД!");
			System.exit(0);
		}
	}
	
	public Group getGroup(int groupId) {
		Group g = new Group(groupId);
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = preparedSelects.get("GROUPS");
			st.setInt(1, groupId);
			rs = st.executeQuery();

			while (rs.next()) {
				g.setId(rs.getInt("group_id"));
				g.setName(rs.getString("group_name"));
			}
			
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Не вдалося завантажити групу! " + except);
		}
		return g;
	}
	
	public List<Group> getGroups() {
		List<Group> groups = new ArrayList<Group>();
		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT group_id FROM groups");
			while(rs.next()) {
				groups.add(getGroup(rs.getInt("group_id")));
			}
		} catch (SQLException except) {
			JOptionPane.showMessageDialog(null, "Помилка при завантаженні груп!");
		}
		return groups;
	}
	
	/**
	 * Очищає БД
	 * Видаляє всі записи з таблиць
	 */
	public void clearStorage() {
		try {
			for(String tableName : queryManager.getTableNames()) {
				String clearTableQuery = "DELETE FROM " + tableName;
				executeQuery(clearTableQuery);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Не вдалося очистити БД!" + e);
		} catch (QueryNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Не вдалося завантажити список таблиць БД!" + e);
		}
	}
	
	/**
	 * Чи існує база данних з іменем database
	 * Вважається, що БД існує, коли є файл з БД
	 * 
	 * @param database ім'я БД
	 * @return true, коли БД існує
	 */
	public static boolean exists(String database) {
		File dbFile = new File(database + ".h2.db");
		return dbFile.exists();
	}
	
	/**
	 * Видаляє БД з іменем database
	 * 
	 * @param database назва БД для видалення
	 * 
	 * @throws SQLException
	 */
	public static void delete(String database) throws SQLException {
		String queryToDelete = "DROP ALL OBJECTS DELETE FILES";
		Storage storageToDelete = Storage.getInstance(database);
		
		storageToDelete.executeQuery(queryToDelete);
		instances.clear();
		if(!storageToDelete.connection.isClosed()) {
			storageToDelete.connection.close();
		}
	}
	
	@Override
	public String toString() {
		return databaseFileName;
	}
}

