package org.ejournal.db.query;

import java.util.List;

/**
 * Інтерфейс для представлення абстрактного сховища запитів до БД
 * Ціль - розділення текстів запитів від основної логіки программи
 * Всі методи можуть збуджувати виключення QueryNotFoundException у випадку помилки при знаходженні запиту
 * 
 * @author Мельничук Іван Володимирович <b>1nt3g3r</b>
 * 
 * @see QueryNotFoundException
 */

public interface QueryManager {
	/**
	 * Повертає запит на створення таблиці
	 * 
	 * @param tableName назва таблиці
	 * 
	 * @return запит на створення таблиці
	 * 
	 * @throws QueryNotFoundException 
	 */
	public String getCreateTableQuery(String tableName) throws QueryNotFoundException;
	
	/**
	 * Повертає запит на оновлення існуючих даних в таблиці
	 * Цей запит необхідно підставити в PreparedStatement і замінити необхідні знаки питання на значення
	 * 
	 * @param tableName назва таблиці, де необхідно оновити дані
	 * 
	 * @return запит на оновлення таблиці
	 * 
	 * @throws QueryNotFoundException 
	 */
	public String getUpdateQuery(String tableName) throws QueryNotFoundException;
	
	/**
	 * Повертає запит на добавлення нових даних в таблицю
	 * Цей запит необхідно підставити в PreparedStatement і замінити необхідні знаки питання на значення
	 * 
	 * @param tableName назва таблиці, куди необхідно вставити дані
	 * 
	 * @return запит на додання нових даних до таблиці
	 * 
	 * @throws QueryNotFoundException 
	 */
	public String getInsertQuery(String tableName) throws QueryNotFoundException;
	
	/**
	 * Повертає запит на вибірку даних із таблиці
	 * Цей запит необхідно підставити в PreparedStatement i замінити необхідні знаки питання на значення
	 * 
	 * @param tableName назва таблиці
	 * 
	 * @return запит на вибірку данних з таблиці
	 * 
	 * @throws QueryNotFoundException
	 */
	public String getSelectQuery(String tableName) throws QueryNotFoundException;
	
	/**
	 * Повертає запит на видалення даних із таблиці
	 * Цей запит необхідно підставити в PreparedStatement і замінити необхідні знаки питання на значення
	 * 
	 * @param tableName назва таблиці
	 * 
	 * @return запит на видалення даних із таблиці
	 * 
	 * @throws QueryNotFoundException
	 */
	public String getDeleteQuery(String tableName) throws QueryNotFoundException;
	
	/**
	 * Повертає список всіх таблиць, які мають бути доступні в БД
	 * 
	 * @return коллекцію назв таблиць, які мають бути в БД
	 * 
	 * @throws QueryNotFoundException 
	 */
	public List<String> getTableNames() throws QueryNotFoundException;
	
	/**
	 * Повертає запит для створення таблиці-довідника
	 * Таблиця-довідник - це таблиця лише із двома полями - Id, Name
	 * Для детальніших данних дивіться comission.logic.book.Book
	 * 
	 * @param bookName ім'я таблиці-довідника
	 * 
	 * @return запит для створення таблиці-довідника
	 * 
	 * @see Book
	 */
	public String getCreateBookTableQuery(String bookName);
	
	/**
	 * Повертає список всіх довідників, необхідних для БД
	  * Таблиця-довідник - це таблиця лише із двома полями - Id, Name
	 * Для детальніших данних дивіться comission.logic.book.Book
	 * 
	 * @return коллекцію назв всіх довідників
	 * 
	 * @throws QueryNotFoundException 
	 * 
	 * @see Book
	 */
	public List<String> getBookTableNames() throws QueryNotFoundException;
	
	/**
	 * Повертає запит на створення нового запису в книзі
	 * 
	 * @param bookName назва книги, куди необхідно добавити запис
	 * 
	 * @return запит на додання нового запису
	 * 
	 * @see Book
	 */
	public String getQueryForInsertIntoBook(String bookName);
}
