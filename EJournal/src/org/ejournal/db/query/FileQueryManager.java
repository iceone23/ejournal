package org.ejournal.db.query;

import java.awt.print.Book;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.ejournal.db.FileTextLoader;


/**
 * Клас для відокремлення SQL-запитів від основної логіки программи
 * Основна ідея в тому, що запити зберігаються в файлах, що дозволяє модифікувати їх без перекомпіляції программи
 * 
 * Для даного класу був створений тестовий клас
 * 
 * @author Мельничук Іван Володимирович <b>1nt3g3r</b>
 * 
 * @see QueryMangerInterface
 * @see FileQueryManagerTest
 */
public class FileQueryManager implements QueryManager {
	private String basePath;
	
	public FileQueryManager(String basePath) {
		this.basePath = basePath;
	}
	/**
	 * Повертає запит на створення нової таблиці
	 * В каталозі з программою має бути створений деякий каталог, який ми назвемо базовим
	 * В базовому каталозі знаходиться каталог sql, в якому знаходиться каталог create
	 * В каталозі create має знаходитись файл Create_table_name.sql, де замість символів
	 * table_name - назва таблиці
	 * В цьому файлі має бути запит на створення таблиці
	 * 
	 * Приклад
	 * getCreateTableQuery(test_table) ->> CREATE TABLE IF NOT EXISTS test_table;
	 * Файл ->> basePath/sql/create/Create_test_table.sql
	 * Вміст файлу - CREATE TABLE IF NOT EXISTS test_table
	 * У випадку відсутності файлу повертає пусту стрічку
	 * 
	 * @param tableName - назва таблиці
	 * @param basePath - базова адреса, де потрібно шукати каталог sql
	 * 
	 * @return вираз для створення цієї таблиці
	 */
	public String getCreateTableQuery(String tableName) throws QueryNotFoundException {
		String query = "";
		String fullPathToQuery = basePath + "sql" + File.separator + "create" + File.separator + "Create_" + tableName +".sql";
		try {
			for(String queryPart : FileTextLoader.getLinesFromFile(fullPathToQuery)) {
				query += queryPart;
				query += " ";
			}
		} catch (FileNotFoundException fileNotFound) {
			throw new QueryNotFoundException("Файл із запитом на створення таблиці " + tableName + " не знайдено!");
		} catch (IOException fileReadError) {
			throw new QueryNotFoundException("Не вдається прочитати дані із файла із запитом на створення таблиці " + tableName +"!");
		}
		return query;
	}
	
	
	/**
	 * Повертає список всіх таблиць, які повинні бути в БД
	 * Використовує файл TableList.sql, який повинний знаходитись в каталозі basePath/sql
	 * У випадку відсутності файла повертає пустий список
	 * 
	 * @param basePath - базова адреса, де потрібно шукати каталог sql
	 * 
	 * @return коллекцію з назвами всіх таблиць, які мають бути в БД
	 */
	public List<String> getTableNames()  throws QueryNotFoundException{
		String fullPathToTableList = basePath+"sql" +File.separator+"TableList.sql";
		List<String> tableNames = null;
		try {
			tableNames = FileTextLoader.getLinesFromFile(fullPathToTableList);
		} catch (FileNotFoundException fileNotFound) {
			throw new QueryNotFoundException("Файл із списком таблиць не знайдено!");
		} catch (IOException fileReadError) {
			throw new QueryNotFoundException("Не вдається прочитати дані із файла із таблицями!");
		}
		return tableNames;
	}

	/**
	 * Повертає запит на додання нових даних у таблицю
	 * Запит має бути переданий у PreparedStatement для подальшої обробки
	 *
	 * @param tableName назва таблиці для додання нових даних
	 * 
	 * @return запит для додання нових даних у таблицю
	 * 
	 * @throws QueryNotFoundException
	 */
	public String getInsertQuery(String tableName) throws QueryNotFoundException {
		String query = "";
		String fullPathToQuery = basePath + "sql" + File.separator + "insert" + File.separator + "Insert_" + tableName +".sql";
		try {
			for(String queryPart : FileTextLoader.getLinesFromFile(fullPathToQuery)) {
				query += queryPart;
				query += " ";
			}
		} catch (FileNotFoundException fileNotFound) {
			throw new QueryNotFoundException("Файл із запитом на додання нових даних в таблицю " + tableName + " не знайдено!");
		} catch (IOException fileReadError) {
			throw new QueryNotFoundException("Не вдається прочитати дані із файла із запитом на додання нових даних у таблицю " + tableName +"!");
		}
		return query;
	}

	
	/**
	 * Повертає запит на оновлення існуючих данних в таблиці
	 * Запит має бути переданий у PreparedStatement для подальшої обробки
	 *
	 * @param tableName назва таблиці для оновлення існуючих даних у таблиці
	 * 
	 * @return запит для оновлення існуючих даних у таблиці
	 * 
	 * @throws QueryNotFoundException
	 */
	public String getUpdateQuery(String tableName) throws QueryNotFoundException {
		String query = "";
		String fullPathToQuery = basePath + "sql" + File.separator + "update" + File.separator + "Update_" + tableName +".sql";
		try {
			for(String queryPart : FileTextLoader.getLinesFromFile(fullPathToQuery)) {
				query += queryPart;
				query += " ";
			}
		} catch (FileNotFoundException fileNotFound) {
			throw new QueryNotFoundException("Файл із запитом на оновлення існуючих даних в таблиці " + tableName + " не знайдено!");
		} catch (IOException fileReadError) {
			throw new QueryNotFoundException("Не вдається прочитати дані із файла із запитом на оновлення існуючих даних в таблиці " + tableName +"!");
		}
		return query;
	}
	
	/**
	 * Повертає список таблиць-довідників для БД
	 * Список таблиць-довідників має бути в каталозі base_path/sql/BookList.sql
	 * 
	 * @return коллекцію з назвами таблиць-довідників
	 * 
	 * @see Book
	 */
	public List<String> getBookTableNames() throws QueryNotFoundException {
		String fullPathToTableList = basePath+"sql" +File.separator+"BookList.sql";
		List<String> bookNames = null;
		try {
			bookNames = FileTextLoader.getLinesFromFile(fullPathToTableList);
		} catch (FileNotFoundException fileNotFound) {
			throw new QueryNotFoundException("Файл із списком довідників не знайдено!");
		} catch (IOException fileReadError) {
			throw new QueryNotFoundException("Не вдається прочитати дані із файла із довідниками!");
		}
		return bookNames;
	}
	
	/**
	 * Повертає запит для створення таблиці-довідника
	 * 
	 * @return запит для створення таблиці-довідника
	 * 
	 * @param назва таблиці-довідника
	 * 
	 * @see Book
	 */
	public String getCreateBookTableQuery(String bookName) {
		if (bookName.equals("SPECIAL")) {
			return "CREATE TABLE IF NOT EXISTS " + bookName + "_BOOK_TABLE ( \n" +
					bookName + "_ID INT PRIMARY KEY, \n" +
					bookName + "_NAME VARCHAR(255), \n" +
					bookName + "_CODE VARCHAR(255)\n" +
					")";
		}
		
		if (bookName.equals("INDEX")) {
			return "CREATE TABLE IF NOT EXISTS INDEX_BOOK_TABLE ( \n" +
					" INDEX_CODE INT PRIMARY KEY, \n" +
					" INDEX_REGION VARCHAR(255), \n" +
					" INDEX_AREA VARCHAR(255), \n" +
					" INDEX_CITY VARCHAR(255)\n" +
					")";
		}
		
		return "CREATE TABLE IF NOT EXISTS " + bookName + "_BOOK_TABLE ( \n" +
				bookName + "_ID INT PRIMARY KEY, \n" +
				bookName + "_NAME VARCHAR(255)\n" +
				")";
	}
	
	/**
	 * Повертає запит на вставку даних в таблицю
	 * Цей запит має бути переданий в PreparedStatement для подальшої заміни знаків питання на потрібні значення
	 * 
	 * @return запит на вставку даних в таблицю
	 * 
	 * @param tableName назва таблиці, в яку будуть вставлені дані
	 */
	public String getSelectQuery(String tableName) throws QueryNotFoundException {
		String query = "";
		String fullPathToQuery = basePath + "sql" + File.separator + "select" + File.separator + "Select_" + tableName +".sql";
		try {
			for(String queryPart : FileTextLoader.getLinesFromFile(fullPathToQuery)) {
				query += queryPart;
				query += " ";
			}
		} catch (FileNotFoundException fileNotFound) {
			throw new QueryNotFoundException("Файл із запитом на вибірку даних із таблиці " + tableName + " не знайдено!");
		} catch (IOException fileReadError) {
			throw new QueryNotFoundException("Не вдається прочитати дані із файла із запитом на вибірку даних з таблиці " + tableName +"!");
		}
		return query;
	}
	
	/**
	 * Повертає запит на видалення даних з таблиці
	 * Цей запит має бути переданий в PreparedStatement для подальшої заміни знаків питання на потрібні значення
	 * 
	 * @return запит на видалення даних із таблиці
	 * 
	 * @param tableName назва таблиці, з якої будуть видалені дані
	 */
	public String getDeleteQuery(String tableName) throws QueryNotFoundException {
		String query = "";
		String fullPathToQuery = basePath + "sql" + File.separator + "delete" + File.separator + "Delete_" + tableName +".sql";
		try {
			for(String queryPart : FileTextLoader.getLinesFromFile(fullPathToQuery)) {
				query += queryPart;
				query += " ";
			}
		} catch (FileNotFoundException fileNotFound) {
			throw new QueryNotFoundException("Файл із запитом на видалення даних із таблиці " + tableName + " не знайдено!");
		} catch (IOException fileReadError) {
			throw new QueryNotFoundException("Не вдається прочитати дані із файла із запитом на видалення даних з таблиці " + tableName +"!");
		}
		return query;
	}
	
	public String getQueryForInsertIntoBook(String bookName) {
		return "INSERT INTO " + bookName + "_BOOK_TABLE (" + 
				bookName + "_ID, " + bookName + "_NAME) VALUES(?, ?)";
	}
}
