package org.ejournal.db.query;

/**
 * Клас для виключення, коли запит по якій-небудь причині не може бути повернений
 * 
 * @author Мельничук Іван Володимирович <b>1nt3g3r</b>
 */
public class QueryNotFoundException extends Exception {
	private static final long serialVersionUID = -7557435672211617166L;
	private String message;
	
	/**
	 * Конструктор, отримує один параметр з інформацією про помилку
	 * 
	 * @param message інформація про помилку
	 */
	public QueryNotFoundException(String message) {
		this.message = message;
	}
	
	/**
	 * Повертає текст помилки
	 */
	public String toString() {
		return message;
	}
}
