package org.ejournal.db;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileTextLoader {
	public static List<String> getLinesFromFile(String filename) throws IOException, FileNotFoundException{
		List<String> fileLines = new ArrayList<String>();
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		
		try {
			reader = new FileReader(filename);
			bufferedReader = new BufferedReader(reader);
			while(bufferedReader.ready()) {
				fileLines.add(bufferedReader.readLine());
			}
		} catch (FileNotFoundException fileNotFound) {
			throw fileNotFound;
		} catch(IOException fileReadError) {
			throw fileReadError;
		} finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fileLines;
	}
}
