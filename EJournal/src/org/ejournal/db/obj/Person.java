package org.ejournal.db.obj;


public class Person {

	private int id;
	private String name;
	private int groupId;
	
	public Person() {
	}
	
	public Person(int personId) {
		this.id = personId;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	
}
