package org.ejournal.db.obj;

public class Group {
	
	private int id;
	private String name;
	
	public Group() {
	}
	
	public Group(int groupId) {
		this.id = groupId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
