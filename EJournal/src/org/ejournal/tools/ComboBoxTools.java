package org.ejournal.tools; 

import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 * Клас для різноманітних операцій з випадаючими списками
 * 
 * @author Мельничук Іван Володимирович <b>1nt3g3r</b>
 */
public class ComboBoxTools {
	/**
	 * Встановлює елементи для випадаючого списку. Також добавляє елемент у вигляді пустої стрічки
	 * 
	 * @param items елементи для встановлення
	 * @param box випадаючий список, куди будуть поміщені елементи
	 */
	public static void setItemsForComboBox(List<String> items, JComboBox box) {
		String[] stringItems = new String[items.size()];
		int stringItemIndex = 0;
		
		for(String item : items) {
			stringItems[stringItemIndex] = item;
			stringItemIndex++;
		}
		
		setItemsForComboBox(stringItems, box);
	}
	
	/**
	 * Вибирає активний елемент для випадаючого списку. Якщо такого елемента немає в списку, то
	 * вибирається елемент у вигляді пустої стрічки.
	 * 
	 * @param selectedItem активний елемент
	 * @param box випадаючий список
	 */
	public static void setSelectedItemForComboBox(String selectedItem, JComboBox box) {
		DefaultComboBoxModel model = (DefaultComboBoxModel) box.getModel();
		int selectedIndex = model.getIndexOf(selectedItem);
		if(selectedIndex >= 0) {
			box.setSelectedIndex(selectedIndex);
		} else {
			box.setSelectedItem("");
		}
	}
	
	/**
	 * Очищає випадаючий список
	 * @param box список для очищення
	 */
	public static void clearComboBox(JComboBox box) {
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		box.setModel(model);
	}

	/**
	 * Встановлює елементи для випадаючого списку. Також добавляє елемент у вигляді пустої стрічки
	 * 
	 * @param items елементи для встановлення
	 * @param box випадаючий список, куди будуть поміщені елементи
	 */
	public static void setItemsForComboBox(String[] items, JComboBox box) {
		DefaultComboBoxModel model = new DefaultComboBoxModel(items);
		model.insertElementAt("", 0);
		box.setModel(model);
	}
}
