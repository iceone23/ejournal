package org.ejournal;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.ejournal.ui.EJournalFrame;

/**
 * Точка входу в програму. Створює і запускає екземпляр класу.
 * EnterToSystem в окремому потоці SwingUtilities.
 * 
 * @author Мельничук Іван Володимирович <b>1nt3g3r</b>
 */
public class Start {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame window = new EJournalFrame();
				window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				window.setVisible(true);
			}
		});
	}
}
